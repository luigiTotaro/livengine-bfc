angular.module('LiveEngine.DeviceLog.Services', [])

.service('DeviceLogService', function($rootScope, $http, $ionicModal, $ionicPopup, CryptoService, FileSystemService, GlobalVariables, $httpParamSerializerJQLike) {

      var me = this;



      me.log = function(level, message) {
        console.log("LOG: " + level + " - " + message);

        //lo posso loggare? OFF, ERROR, WARN, INFO
        if (GlobalVariables.logLevel=="OFF") return; //non scrivo niente
        else if (GlobalVariables.logLevel=="ERROR")
        {
          if ((level=="WARN") || (level=="INFO")) return;
        }
        else if (GlobalVariables.logLevel=="WARN")
        {
          if (level=="INFO") return;
        }
        //altrimenti il livello è INFO e devo loggare tutto

        var LOG_FILE_NAME = "log.txt";
        FileSystemService.fileRead({
            directory: GlobalVariables.bufferDirectory,
            fileName: LOG_FILE_NAME,
            onComplete: function(err, readData) {

                var log = new Array();
                if(err || !readData) {
                    //probabile mancanza del file di log, primo uso di questa versione dell'app


                } else {
                    var decoded = CryptoService.decode(readData);
                    try {
                      log = JSON.parse(decoded);
                    } catch (e) { //se non riesce a parsare il file non è consistente, lo annullo
                      log = new Array();
                    }

                }

                //aggiungo il log
                var data = new Date();
                var mm = data.getMonth() + 1; // getMonth() is zero-based
                var dd = data.getDate();
                var hh = data.getHours();
                var min = data.getMinutes();
                var ss = data.getSeconds();
                var dataFormat = data.getFullYear() + "-" + (mm>9 ? '' : '0') + mm + "-" + (dd>9 ? '' : '0') + dd + " " + (hh>9 ? '' : '0') + hh + ":" + (min>9 ? '' : '0') + min + ":" +(ss>9 ? '' : '0') + ss;
                  
                log2add=new Object();
                log2add.data = dataFormat;
                log2add.level = level;
                log2add.msg = message;
                log.push(log2add);

                //aggiorno il log sul device
                var logStr = JSON.stringify(log);
                var encoded = CryptoService.encode(logStr);

                FileSystemService.writeFile({
                    directory: GlobalVariables.bufferDirectory,
                    fileName: LOG_FILE_NAME,
                    bufferData: encoded,
                    onComplete: function(err, operationCompleted) {

                        if(err || !operationCompleted) {
                          console.log("Errore nella scrittura del File di log");
                        }
                        else
                        {
                            console.log("File log scritto correttamente");
                            //se era un log di errore, provo a inviarlo subito
                            if (level=="ERROR")
                            {
                                me.sendLogFile({
                                    onComplete: function(err, res) {

                                        if(err) {
                                            console.log("Errore nell'invio del file di log");
                                            console.log(err);
                                        } else {
                                            console.log("File di log inviato correttamente");
                                        }

                                    }
                                });                            
                            }


                        }

                    } 
                });               

            }
        });        

      };


      me.sendLogFile = function(parameters) {

        console.log("sono in sendLogFile");
        //prendo il log aggiornato dal filesystem
        var LOG_FILE_NAME = "log.txt";

        FileSystemService.fileRead({
            directory: GlobalVariables.bufferDirectory,
            fileName: LOG_FILE_NAME,
            onComplete: function(err, readData) {

                if(err || !readData) {
                    console.log("ERRORE LETTURA FILE LOG");
                    parameters.onComplete("ERRORE LETTURA FILE LOG", null);
                } else {
                    var decoded = CryptoService.decode(readData);
                    console.log("log: " + decoded);
                    // ci sta qualcosa o è vuoto?
                    if (decoded.length==0)
                    {
                      console.log("FILE LOG VUOTO");
                      parameters.onComplete("FILE LOG VUOTO", null);
                    } 
                    else
                    {
                      //decoded = decoded.substring(0, 100);
                      //invio
                      console.log("invio");
                      var xsrf = $httpParamSerializerJQLike({
                            uuid: GlobalVariables.deviceUUID,
                            bundleId: GlobalVariables.packageName,
                            log: decoded
                          });

                      $http({
                          url: GlobalVariables.baseUrl + "/logDevices.php",
                          method: 'POST',
                          timeout: 10000,
                          data: xsrf,
                          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                      })
                      .then(function(json) {
                          console.log("ritorno ok: " + JSON.stringify(json));

                          //svuoto il file di log
                          FileSystemService.writeFile({
                              directory: GlobalVariables.bufferDirectory,
                              fileName: LOG_FILE_NAME,
                              bufferData: "",
                              onComplete: function(err, operationCompleted) {

                                //ritorno
                                parameters.onComplete(null, null);

                              } 
                          });                           


                      }, function(err) {
                          console.log("ritorno con errore: " + JSON.stringify(err));
                          //ritorno
                          parameters.onComplete(err, null);
                      });
                    }
                }

            }
        });        



        

    }      




})

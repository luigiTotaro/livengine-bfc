angular.module('LiveEngine.WikiPrincipale.Services', [])

.service('WikiService_principale', function(LoggerService, HelperService, $rootScope, $http, $ionicModal, $ionicPopup, $ionicLoading, GlobalVariables, FileSystemService, SupportServices, LazyLoader, WikiService, LanguageService, $state) {

    var me = this;

    //me.toDownload = new Array();

    me.callbacks = [];


    me.registerCallback = function(key, functionName, callback) {

        if(!me.callbacks[key]) {
            me.callbacks[key] = [];
        }

        delete me.callbacks[key][functionName];

        if(callback) {
            me.callbacks[key][functionName] = callback;
        }

    }


    me.loadARchitectWorld = function (example)
    {


        console.log("Entrato in loadARchitectWorld Progetto");

        GlobalVariables.wikitudePlugin.requestAccess(function()
        {
            console.log("Request Access OK");


            // check if the current device is able to launch ARchitect Worlds
            GlobalVariables.wikitudePlugin.isDeviceSupported(function()
            {
                console.log("Device is supported");
               //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);


                GlobalVariables.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
                {
                    //console.log("loadARworld ok, call calljavascript: "+example.localPath);
                    /* Respond to successful world loading if you need to */

                    console.log("dovrei averla chiamata....");

                }, function errorFn(error)
                {
                    console.log("Loading AR web view failed");
                    console.log(error);
                    
                    //alert('Loading AR web view failed');
                    HelperService.showUnrecoverableError(LanguageService.getLabel('ERRORE_AVVIO_RA'), true);
                },
                example.path, example.requiredFeatures, example.startupConfiguration
                );
                GlobalVariables.wikitudePlugin.callJavaScript('passData("'+example.localPath+'","'+example.localDynamicARPath+'","'+example.idPrj+'",'+example.isApiRest+',"'+example.baseUrl+'","'+example.deviceId+'",'+LazyLoader.appConfiguration.showDisclaimerForComments+',"'+LazyLoader.appConfiguration.commentsNickname+'","'+example.multiTarget+'","'+GlobalVariables.systemLanguage+'")');
                
                GlobalVariables.wikitudePlugin.setJSONObjectReceivedCallback(function (objPassed)
                    {
                        console.log("onJSONObjectReceived!!!");
                        console.log(JSON.stringify(objPassed));
                        switch(objPassed.topic) {
                            case "scaricaImmagine":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.TRIGGER_WIKI_EVT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        type: 'download_item',
                                        url: decodeURIComponent(objPassed.url),
                                        descr: objPassed.descr
                                    }
                                });                                
                                break;
                            case "getUsersNumber":
                                var numero=GlobalVariables.application.currentProgetto.numUtenti;
                                GlobalVariables.wikitudePlugin.callJavaScript('getUsersNumber('+numero+')');
                                break;
                            case "checkConnessione":
                                var connessione=HelperService.isNetworkAvailable();
                                console.log("Check connessione: " + connessione);
                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessione('+connessione+')');
                                break;
                            case "openContent":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_AUGMENTED_CONTENT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        id: objPassed.id,
                                        titolo: objPassed.titolo
                                    }
                                });
                                break;
                            case "hotspotClick":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.HOTSPOT_CLICK,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        id: objPassed.id,
                                        titolo: objPassed.titolo,
                                        id_hotspot: objPassed.id_hotspot,
                                        hotspot: objPassed.hotspot,
                                        extraData: objPassed.extraData
                                    }
                                });
                                break;
                            case "linkEsterno":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_EXTERNAL_URL,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        url: decodeURIComponent(objPassed.url)
                                    }
                                });
                                break;

                            case "scaricaSlideshow":
                                //nell'oggetto ci sta l'id del punto e le immagini da scaricare
                                WikiService.toDownload = new Array();
                                var idPunto=objPassed.array[0];
                                for (var index=1;index<objPassed.array.length;index++)
                                {
                                  WikiService.toDownload.push(decodeURIComponent(objPassed.array[index]));
                                }
                                console.log("array da scaricare");
                                console.log(WikiService.toDownload);
                                WikiService.downloadFiles(0,idPunto);
                                break;
                            case "existSlideshow":
                                //esiste, nella directory slideshow, qualche file che inizia con l'id del punto?
                                var name=objPassed.id+"_0"; //almeno il primo
                                FileSystemService.fileExists({
                                    directory: SupportServices.BUFFER_DIRECTORY + '/slideshow',
                                    fileName: name,
                                    onComplete: function(err, isAvailable) {
                                        if(err) {
                                            //errore, getto la spugna
                                            console.log("Errore, comunico che non ci sono files");
                                            GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                        } else {
                                            if(!isAvailable) {
                                                 //non esiste, getto la spugna
                                                console.log("Non Esiste, comunico che non ci sono files");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                            } else {
                                                //esiste, vado avanti
                                                console.log("Esiste");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(true)');
                                            }
                                        }
                                    }
                                });
                                break;
                            case "scaricaPanoImage":
                                //var fileName=objPassed.fileName;
                                //vedo se esiste
                                FileSystemService.fileExists({
                                    directory: SupportServices.BUFFER_DIRECTORY + '/pano',
                                    fileName: objPassed.fileToSave,
                                    onComplete: function(err, isAvailable) {
                                        if(err) {
                                            console.log("err: " + err)
                                            //errore, se ci sta la connessione lo scarico
                                            if (objPassed.connectionPresent)
                                            {
                                                console.log("Non esiste, provo a scaricarlo");
                                                WikiService.downloadPanoFile(decodeURIComponent(objPassed.fileName), objPassed.fileToSave);
                                            }
                                            else
                                            {
                                                console.log("Non esiste e non ho la connessione, rinuncio");
                                                GlobalVariables.wikitudePlugin.callJavaScript('panoReturn(false,null)');
                                            }
                                        } else {
                                            if(!isAvailable) {
                                                 //non esiste, se ci sta la connessione lo scarico
                                                if (objPassed.connectionPresent)
                                                {
                                                    console.log("Non esiste, provo a scaricarlo");
                                                    WikiService.downloadPanoFile(decodeURIComponent(objPassed.fileName), objPassed.fileToSave);
                                                }
                                                else
                                                {
                                                    console.log("Non esiste e non ho la connessione, rinuncio");
                                                    GlobalVariables.wikitudePlugin.callJavaScript('panoReturn(false,null)');
                                                }                                            
                                            } else {
                                                //esiste, ma se ci sta la connessione provo comunque a riscaricarla
                                                console.log("Esiste");
                                                if (objPassed.connectionPresent)
                                                {
                                                    console.log("ma provo comunque a scaricarlo");
                                                    WikiService.downloadPanoFile(decodeURIComponent(objPassed.fileName), objPassed.fileToSave);
                                                }
                                                else
                                                {
                                                    console.log("ma non ho la connessione, allora uso quella che ho");
                                                    GlobalVariables.wikitudePlugin.callJavaScript("panoReturn(true,'"+objPassed.fileToSave+"')");
                                                }                                            
                                            }
                                        }
                                    }
                                });



                                //WikiService.downloadPanoFile(decodeURIComponent(fileName));
                                break;
                            case "writeComment":
                                GlobalVariables.wikitudePlugin.hide();
                                WikiService.showPopupWriteComment(objPassed);
                                break;
                            case "socialPublish":

                                //GlobalVariables.wikitudePlugin.hide();
                                GlobalVariables.wikitudePlugin.close();

                                $ionicLoading.show({
                                    template: '<div style="padding:10px 5px;">Attendere.....</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
                                    duration: 15000
                                });

                                var appParameter="";

                                if (objPassed.type=="fb")
                                {
                                    
                                    var immagine2share=null;
                                    var link2share=null;
                                    if (objPassed.image!="null") immagine2share = decodeURIComponent(objPassed.image);
                                    if (objPassed.link!="null") link2share = decodeURIComponent(objPassed.link);


                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per fb');

                                        if (GlobalVariables.platform=="iOS")
                                        {
                                            //ios - funziona ma restituisce un errore "not available" dopo aver condiviso.
                                            //per ovviare, si vede prima se può condividere con canShareVia e poi se tutto ok si condivide fregandosene del messaggio di errore seguente
                                            window.plugins.socialsharing.canShareVia("com.apple.social.facebook", 'Message via fb', null, null , null, function(e)
                                            {
                                              //tutto ok, può condividere
                                              window.plugins.socialsharing.shareViaFacebook(null, immagine2share, link2share, function() 
                                                {
                                                    console.log('share ok')
                                                    me.showPopupReturn2AR("fb");
                                                }, function(errormsg)
                                                {
                                                    console.log('share ok, se messaggio è not available')
                                                    if (errormsg=="not available") me.showPopupReturn2AR("fb");
                                                    else me.showPopupReturn2AR("fb_error");
                                                });

                                            }, function(e)
                                            {
                                                console.log("KO: " + e);
                                                me.showPopupReturn2AR("fb_error");
                                            });
                                        }
                                        else
                                        {
                                            window.plugins.socialsharing.shareViaFacebook(null, immagine2share, link2share, function() 
                                                {
                                                    console.log('share ok')
                                                    me.showPopupReturn2AR("fb");
                                                }, function(errormsg)
                                                {
                                                    console.log("KO: " + errormsg);
                                                    me.showPopupReturn2AR("fb_error");
                                                });

                                        }
                                    }, 1000);                              


                                    /*
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.facebook";
                                    else appParameter="com.facebook.katana";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per fb');
                                        
                                        // this is the complete list of currently supported params you can pass to the plugin (all optional)
                                        var options = {
                                          //message: null, // not supported on some apps (Facebook, Instagram)
                                          //subject: null, // fi. for email
                                          //files: [decodeURIComponent(objPassed.image)], // an array of filenames either locally or remotely
                                          //url: decodeURIComponent(objPassed.link),
                                          appPackageName: appParameter // Android only, you can provide id of the App you want to share with
                                        };

                                        if (objPassed.image != "null") options.files = [decodeURIComponent(objPassed.image)];
                                        if (objPassed.link != "null") options.url = decodeURIComponent(objPassed.link);

                                        console.log("oggetto per fb:");
                                        console.log(JSON.stringify(options));


                                        var onSuccess = function(result) {
                                            console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
                                            console.log("Shared to app: " + result.app); // On Android result.app since plugin version 5.4.0 this is no longer empty. On iOS it's empty when sharing is cancelled (result.completed=false)
                                            me.showPopupReturn2AR("fb", this);
                                        };

                                        var onError = function(msg) {
                                          console.log("Sharing failed with message: " + msg);
                                            me.showPopupReturn2AR("fb", this);
                                        };

                                        window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
*/


/*

                                        window.plugins.socialsharing.canShareVia(appParameter, objPassed.text, decodeURIComponent(objPassed.image), decodeURIComponent(objPassed.link), null, function(e)
                                        {
                                            console.log("success: " + e)
                                            console.log("parametri da passare alla funzione di share:");
                                            console.log("image: " + decodeURIComponent(objPassed.image));
                                            console.log("link: " + decodeURIComponent(objPassed.link));

                                            //window.plugins.socialsharing.shareViaFacebook(null, decodeURIComponent(objPassed.image), decodeURIComponent(objPassed.link), function()
                                            window.plugins.socialsharing.shareVia(appParameter, null, decodeURIComponent(objPassed.image), decodeURIComponent(objPassed.link), null, function()
                                            {
                                                console.log('share ok fb');
                                                WikiService.showPopupReturn2AR("fb");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore fb: ' + errormsg);
                                                WikiService.showPopupReturn2AR("fb");
                                            });
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("fb_error");
                                            $ionicLoading.hide();
                                        });
*/
/*
                                    }, 1000);                              
*/
                                }
                                else if (objPassed.type=="tw")
                                {

                                    var testo2share=null;
                                    var immagine2share=null;
                                    var link2share=null;
                                    if (objPassed.text!="null") testo2share = decodeURIComponent(objPassed.text);
                                    if (objPassed.image!="null") immagine2share = decodeURIComponent(objPassed.image);
                                    if (objPassed.link!="null") link2share = decodeURIComponent(objPassed.link);


                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per tw');

                                        if (GlobalVariables.platform=="iOS")
                                        {
                                            //ios - funziona ma restituisce un errore "not available" dopo aver condiviso.
                                            //per ovviare, si vede prima se può condividere con canShareVia e poi se tutto ok si condivide fregandosene del messaggio di errore seguente
                                            window.plugins.socialsharing.canShareVia("com.apple.social.twitter", 'Message via tw', null, null , null, function(e)
                                            {
                                              //tutto ok, può condividere
                                              window.plugins.socialsharing.shareViaTwitter(testo2share, immagine2share, link2share, function() 
                                                {
                                                    console.log('share ok')
                                                    me.showPopupReturn2AR("tw");
                                                }, function(errormsg)
                                                {
                                                    console.log('share ok, se messaggio è not available')
                                                    if (errormsg=="not available") me.showPopupReturn2AR("tw");
                                                    else me.showPopupReturn2AR("tw_error");
                                                });

                                            }, function(e)
                                            {
                                                console.log("KO: " + e);
                                                me.showPopupReturn2AR("tw_error");
                                            });
                                        }
                                        else
                                        {
                                            window.plugins.socialsharing.shareViaTwitter(testo2share, immagine2share, link2share, function() 
                                                {
                                                    console.log('share ok')
                                                    me.showPopupReturn2AR("tw");
                                                }, function(errormsg)
                                                {
                                                    console.log("KO: " + errormsg);
                                                    me.showPopupReturn2AR("tw_error");
                                                });

                                        }
                                    }, 1000);   


/*

                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.twitter";
                                    else appParameter="twitter";

                                     setTimeout(function() {
                                        console.log('chiamo il plugin di share per tw');
                                        window.plugins.socialsharing.canShareVia(appParameter, decodeURIComponent(objPassed.text), null, decodeURIComponent(objPassed.image), null, function(e)
                                        {
                                            console.log("success: " + e);
                                            //passare null dove non utilizzato
                                            window.plugins.socialsharing.shareViaTwitter(decodeURIComponent(objPassed.text), decodeURIComponent(objPassed.image), decodeURIComponent(objPassed.link), function()
                                            {
                                                console.log('share ok tw');
                                                WikiService.showPopupReturn2AR("tw");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore tw: ' + errormsg);
                                                WikiService.showPopupReturn2AR("tw");
                                            });  
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("tw_error");
                                            $ionicLoading.hide();
                                        });
                                        console.log("dovrei aver fatto");
                                    }, 1000);

*/

                                }
                                else if (objPassed.type=="insta")
                                {

                                    if (GlobalVariables.platform=="iOS") appParameter="instagram";
                                    else appParameter="instagram";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per instagram');
                                        window.plugins.socialsharing.canShareVia(appParameter, decodeURIComponent(objPassed.text), null, decodeURIComponent(objPassed.image), null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaInstagram(decodeURIComponent(objPassed.text), decodeURIComponent(objPassed.image), function()
                                            {
                                              console.log('share ok insta');
                                                WikiService.showPopupReturn2AR("insta");
                                                //GlobalVariables.wikitudePlugin.show();
                                            }, function(errormsg)
                                            {
                                              console.log('Errore insta: ' + errormsg);
                                                WikiService.showPopupReturn2AR("insta");
                                                 //GlobalVariables.wikitudePlugin.show();
                                           });                               
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("insta_error");
                                            $ionicLoading.hide();
                                        });
                                     }, 1000);                              
                                }
                                break;
                            case "salvaConfig": //viene chiamata dalla classe dinamica Commento_xxxxx, ma al momento è commentata dappertutto
                                /*
                                LazyLoader.appConfiguration.showDisclaimerForComments = parse.parameter.showCommentsDisclaimer;
                                LazyLoader.appConfiguration.commentsNickname = parse.parameter.commentsNickname;
                                LazyLoader.saveConfiguration({
                                    onComplete: function(err) {
                                    }
                                })
                                */
                                break;                                
                            default:
                                console.log("Esco dalla RA");
                                try{
                                    GlobalVariables.wikitudePlugin.close();
                                    console.log("dovrei averla chiusa");
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                catch(err){
                                    console.log(err);
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                        }

                    }); 
                
                GlobalVariables.wikitudePlugin.setOnUrlInvokeCallback(function (url)
                    {
                        var parse = WikiService.parseWikitudeCallback(url);
                        switch(parse.value) {
                            case "storage":
                                break;
                            /*
                            case "scaricaImmagine":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.TRIGGER_WIKI_EVT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        type: 'download_item',
                                        url: parse.parameter.url,
                                        descr: parse.parameter.descr
                                    }
                                });
                                break;
                            */
                            /*
                            case "salvaConfig": //viene chiamata dalla classe dinamica Commento_xxxxx, ma al momento è commentatat dappertutto
                                LazyLoader.appConfiguration.showDisclaimerForComments = parse.parameter.showCommentsDisclaimer;
                                LazyLoader.appConfiguration.commentsNickname = parse.parameter.commentsNickname;
                                LazyLoader.saveConfiguration({
                                    onComplete: function(err) {
                                    }
                                })
                                break;
                            */
                            /*
                            case "linkEsterno":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_EXTERNAL_URL,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        url: parse.parameter
                                    }
                                });
                                break;
                            */
                            /*
                            case "checkConnessione":
                                var connessione=HelperService.isNetworkAvailable();
                                console.log("Check connessione: " + connessione);
                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessione('+connessione+')');
                                break;
                            */
                            /*
                            case "getUsersNumber":
                                var numero=GlobalVariables.application.currentProgetto.numUtenti;
                                GlobalVariables.wikitudePlugin.callJavaScript('getUsersNumber('+numero+')');
                                break;
                            */
                            /*
                            case "scaricaSlideshow":
                                //nell'oggetto ci sta l'id del punto e le immagini da scaricare
                                WikiService.toDownload = new Array();
                                var idPunto=parse.parameter[0];
                                for (var index=1;index<parse.parameter.length;index++)
                                {
                                  WikiService.toDownload.push(parse.parameter[index]);
                                }
                                console.log("array da scaricare");
                                console.log(WikiService.toDownload);
                                WikiService.downloadFiles(0,idPunto);
                                break;
                            case "existSlideshow":
                                //esiste, nella directory slideshow, qualche file che inizia con l'id del punto?
                                var name=parse.parameter+"_0"; //almeno il primo
                                FileSystemService.fileExists({
                                    directory: SupportServices.BUFFER_DIRECTORY + '/slideshow',
                                    fileName: name,
                                    onComplete: function(err, isAvailable) {
                                        if(err) {
                                            //errore, getto la spugna
                                            console.log("Errore, comunico che non ci sono files");
                                            GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                        } else {
                                            if(!isAvailable) {
                                                 //non esiste, getto la spugna
                                                console.log("Non Esiste, comunico che non ci sono files");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                            } else {
                                                //esiste, vado avanti
                                                console.log("Esiste");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(true)');
                                            }
                                        }
                                    }
                                });
                                break;
                            */
                            /*
                            case "openContent":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_AUGMENTED_CONTENT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        id: parse.parameter.id,
                                        titolo: parse.parameter.titolo
                                    }
                                });
                                break;
                            */
                            /*
                            case "socialPublish":
                                var oggetto=parse.parameter;

                                GlobalVariables.wikitudePlugin.hide();
                                
                                $ionicLoading.show({
                                    template: '<div style="padding:10px 5px;">Attendere.....</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
                                    duration: 15000
                                });

                                var appParameter="";

                                if (oggetto.type=="fb")
                                {
                                    
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.facebook";
                                    else appParameter="com.facebook.katana";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per fb');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                           window.plugins.socialsharing.shareViaFacebook(null, oggetto.image, oggetto.link, function()
                                            {
                                                console.log('share ok fb');
                                                WikiService.showPopupReturn2AR("fb");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore fb: ' + errormsg);
                                                WikiService.showPopupReturn2AR("fb");
                                            });
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("fb_error");
                                            $ionicLoading.hide();
                                        });
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="tw")
                                {
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.twitter";
                                    else appParameter="twitter";

                                     setTimeout(function() {
                                        console.log('chiamo il plugin di share per tw');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e);
                                            //passare null dove non utilizzato
                                            window.plugins.socialsharing.shareViaTwitter(oggetto.text, oggetto.image, oggetto.link, function()
                                            {
                                                console.log('share ok tw');
                                                WikiService.showPopupReturn2AR("tw");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore tw: ' + errormsg);
                                                WikiService.showPopupReturn2AR("tw");
                                            });  
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("tw_error");
                                            $ionicLoading.hide();
                                        });
                                        console.log("dovrei aver fatto");
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="insta")
                                {

                                    if (GlobalVariables.platform=="iOS") appParameter="instagram";
                                    else appParameter="instagram";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per instagram');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaInstagram(oggetto.text, oggetto.image, function()
                                            {
                                              console.log('share ok insta');
                                                WikiService.showPopupReturn2AR("insta");
                                                //GlobalVariables.wikitudePlugin.show();
                                            }, function(errormsg)
                                            {
                                              console.log('Errore insta: ' + errormsg);
                                                WikiService.showPopupReturn2AR("insta");
                                                 //GlobalVariables.wikitudePlugin.show();
                                           });                               
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("insta_error");
                                            $ionicLoading.hide();
                                        });
                                     }, 1000);                              
                                }
                                break;
                                */
                            case "writeComment":
                                /*
                                GlobalVariables.wikitudePlugin.hide();
                                WikiService.showPopupWriteComment(parse.parameter);
                                break;
                                */
                            default:
                                /*
                                console.log("Esco dalla RA");
                                try{
                                    GlobalVariables.wikitudePlugin.close();
                                    console.log("dovrei averla chiusa");
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                catch(err){
                                    console.log(err);
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                */
                        }
                    });


            }, function(errorMessage)
            {
                console.log("Device is NOT supported");
                //alert(errorMessage);
                HelperService.showUnrecoverableError(LanguageService.getLabel('DEVICE_NON_SUPPORTATO'), true);
            },
            example.requiredFeatures
            );

        }, function(errorMessage)
        {
            console.log(errorMessage);
            //alert("Non riesco ad accedere alla Fotocamera");
            HelperService.showUnrecoverableError(LanguageService.getLabel('NO_ACCESSO_FOTOCAMERA'), true);
        },
        example.requiredFeatures
        );



    }

    me.showPopupReturn2AR = function(social)
    {
        $rootScope.closePopupScreen = function() {
            $rootScope.popupScreen.remove();
            var localPath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;
            var localDynamicARPath = FileSystemService.getLocalPath() + GlobalVariables.directory_main + "/dinamic_classes" + GlobalVariables.directory_trail;
            var idPrj = GlobalVariables.application.currentProgetto.idProgetto;
            var baseUrl = GlobalVariables.baseUrl;
            //var parameter = { "path": "www/clientRecognition/index.html", "requiredFeatures": ["2d_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "idPrj": idPrj, "isApiRest": false, "baseUrl": baseUrl, "deviceId": GlobalVariables.deviceUUID};
            var parameter = { "path": "www/clientRecognition/index.html", "requiredFeatures": ["image_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localPath, "localDynamicARPath": localDynamicARPath, "idPrj": idPrj, "isApiRest": false, "baseUrl": baseUrl, "deviceId": GlobalVariables.deviceUUID, "multiTarget": GlobalVariables.application.currentTestata.multitarget};

            console.log(JSON.stringify(parameter));
            
            //clearInterval($scope.data.timer);
            me.loadARchitectWorld(parameter);            

            //GlobalVariables.wikitudePlugin.show();
        };

        $rootScope.popupScreen = $ionicModal.fromTemplate(


          '<ion-modal-view padding="true" cache-view="false" id="appCreditsScreen" style="width: 100%;height: 100%;top: 0px;left: 0px;">' +

              '<ion-content scroll="true" padding="false" >' +

                  '<div id="logoSocialBox">' +

                  (social == "fb" ? '<img src="img/fbLogo.png" style="width:100%;"></img>' : '') +
                  (social == "tw" ? '<img src="img/twitterLogo.png" style="width:100%;"></img>' : '') +
                  (social == "insta" ? '<img src="img/instagramLogo.png" style="width:100%;"></img>' : '') +
                  
                  (social == "insta_error" ? '<img src="img/instagramLogo.png" style="width:100%;"></img>' : '') +
                  (social == "tw_error" ? '<img src="img/twitterLogo.png" style="width:100%;"></img>' : '') +
                  (social == "fb_error" ? '<img src="img/fbLogo.png" style="width:100%;"></img>' : '') +

                  '</div>'+

                  '<div id="socialInfoBox" style="">' +
                    (social == "insta_error" ? LanguageService.getLabel('SOCIAL_INSTAGRAM_ERROR')+'<br><br>' : '') +
                    (social == "tw_error" ? LanguageService.getLabel('SOCIAL_TWITTER_ERROR')+'<br><br>' : '') +
                    (social == "fb_error" ? LanguageService.getLabel('SOCIAL_FB_ERROR')+'<br><br>' : '') +

                    '<strong>'+LanguageService.getLabel('RETURN_TO_RA')+'</strong>' +

                  '</div>'+

              '</ion-content>' +


              '<ion-footer-bar align-title="left" class="splashScreenFooter">' +
                '<button ng-click="closePopupScreen()" class="button button-full button-positive cst-button" style="background-color: #b41223;margin-top: auto;">'+LanguageService.getLabel('CHIUDI')+'</button>' +
              '</ion-footer-bar>' +

          '</ion-modal-view>',

          {
              scope: $rootScope,
              focusFirstInput: true,
              animation :'none',
              hardwareBackButtonClose: false
          }

        );

        $rootScope.popupScreen.show();
    };
/*
    me.go2ProjectPage = function() {
        $ionicLoading.show({
          template: LanguageService.getLabel('ATTENDERE')
        });

        SupportServices.sceltaProgetto({
          onComplete: function(err, json) {

              $ionicLoading.hide();
              if(err) {

                  var alertPopup = $ionicPopup.alert({
                     title: 'Attenzione',
                     template: '----Sembrano esserci problemi di rete che impediscono la fruizione dell\'applicazione'
                   });

                  return;
              }

              $state.go('progetto_detail');

          }

        });
    };
*/



})

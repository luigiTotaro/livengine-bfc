angular.module('LiveEngine.GeoLocation.Services', [])

.service('GeoLocationService', function(GlobalVariables, LoggerService,
FileSystemService, $rootScope, $http, $ionicModal, $ionicPopup) {

    var me = this;
    me.maximumAge = 3000;
    me.timeout = 30000;
    //me.enableHighAccuracy = false;
    me.enableHighAccuracy = true;
    me.currentPosition = null;
    me.currentIpPosition = null;
    me.currentGeodecodedData = null;
    me.callbacks = [];

    // 30 minuti
    me.watchInterval = 1000 * 60 * 30;

    // 5 secondi
    me.firstRunTimeout = 1000;

    // 15 secondi
    me.onErrorRetry = 15000;

    me.Initialize = function() {

      setTimeout(function() {
          me.watchPosition();
      }, me.firstRunTimeout);

    }

    me.getCurrentGeodecodedData = function() {
      return me.currentGeodecodedData;
    };

    me.getCurrentPosition = function() {
      return me.currentPosition;
    };

    me.setCurrentIpPosition = function(lat,lon) {
      
      currentIpPosition = JSON.parse('{"coords":{"latitude":'+lat+',"longitude":'+lon+',"accuracy":10000,"altitude":0,"heading":-1,"speed":-1,"altitudeAccuracy":1000},"timestamp":0}');
      console.log("---XXX Settato coordinate IP: " + JSON.stringify(currentIpPosition));
    };


    // callback: function(isGpsEnabled, isGpsAuthorized)
    me.checkGpsAvailability = function(callback) {

      if(!window.cordova) {

          callback(me.currentPosition != null, me.currentPosition != null);

      } else {

        
        cordova.plugins.diagnostic.isLocationAuthorized(function(authorized){

            if (authorized)
            {
              cordova.plugins.diagnostic.isLocationEnabled(function(isGpsEnabled) {

                  console.log("--- gpsEnabled: " + isGpsEnabled);
                 callback(isGpsEnabled);

              }, function(error) {
                  callback(false);
              });
            }
            else
            {
              callback(false);
            }

        }, function(error){

            //console.error("The following error occurred: "+error);
            callback(false);
        });

      }


    };


    me.checkCameraPermission = function() {
        
        var reply="";

        cordova.plugins.diagnostic.isCameraAvailable(function(available){
                reply = "Camera is " + (available ? "available" : "not available");
            }, function(error){
                reply = "The following error occurred: "+error;
            }, false
        );
        return reply;
        //return cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED;
    };

    me.checkGPSPermission = function(callback) {

        console.log("GeoLocationService - checkGPSPermission");
        //alert("GeoLocationService - checkGPSPermission");
        if (ionic.Platform.isIOS())
        {
          cordova.plugins.diagnostic.getLocationAuthorizationStatus(function(status){
             //alert(status);
             switch(status){
                case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                  console.log("Permission not requested");
                  callback(true);
                break;
                case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                  console.log("Permission denied");
                  callback(false);
                break;
                case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                  console.log("Permission granted always");
                  callback(true);
                break;
                case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
                  console.log("Permission granted only when in use");
                  callback(true);
                break;
                default:
                  callback(false);                
             }
             
          }, function(error){
              //alert("The following error occurred: "+error);
              console.error("The following error occurred: "+error);
              callback(false);

          });
        }
        else
        {
            cordova.plugins.diagnostic.getLocationAuthorizationStatus(function(status){
                //alert(status);
                switch(status){
                    case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                        console.log("Permission not requested"); //lo da quando hai negato il permesso "non chiedermelo più"
                        callback(true);
                        break;
                    case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                        console.log("Permission granted");
                        callback(true);
                        break;
                    case cordova.plugins.diagnostic.permissionStatus.DENIED_ONCE:
                        console.log("Permission denied");
                        callback(false);
                        break;
                    case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                        console.log("Permission permanently denied");
                        callback(false);
                        break;
                  default:
                    console.log("Permission default: " + status);
                    callback(false);   
                }
            }, function(error){
                console.error("Errore: " + error);
                callback(false);
            });          
        }    
        
    };    

    // quando si assegna una callback, questa viene eseguita subito
    me.setCallback = function(callbackName, callbackFunction) {

        if(me.callbacks[callbackName]) {
            delete me.callbacks[callbackName];
        }

        if(callbackFunction) {

            me.callbacks[callbackName] = callbackFunction;
            me.callbacks[callbackName](me.currentPosition);
        }

    };



    me.onSuccessWatch = function(position) {

        me.currentPosition = position;

        console.log("*****");
        console.log("onSuccessWatch");
        console.log("*****");

        for(var callbackName in me.callbacks) {
            me.callbacks[callbackName](position);
        }

        setTimeout(function() {
            me.watchPosition();
        }, me.watchInterval);

    };


    me.onErrorWatch = function(error) {

        //alert("onErrorWatch");
        //se ci sta un errore, è possibile che sia perchè l'utenta ha negato il permesso al gps, dunque è inutile riprovarci continuamente



        me.checkGPSPermission(function(isPermissionAvailable) {

          //alert("check: " + isPermissionAvailable);

          if (isPermissionAvailable == true)
          {
            //alert("--TRUE");
            setTimeout(function() {
                me.watchPosition();
            }, me.firstRunTimeout);
          }
          else
          {
            //non faccio niente
            //alert("--FALSE");
          }
        

        });

    };


    me.reverseGeocode = function(onComplete) {

        if(me.currentPosition == null) {
          if(me.currentIpPosition == null) {
            onComplete(null,null);
            return;
          }
          me.currentPosition=me.currentIpPosition;
          //onComplete(null,null);
          //return;
        }

        $http({
            url: 'http://nominatim.openstreetmap.org/reverse',
            timeout: 10000,
            params: {
              'format': 'json',
              'lat': me.currentPosition.coords.latitude,
              'lon': me.currentPosition.coords.longitude,
              'zoom': 15,
              'addressdetails':1,
              'accept-language': 'it'
            }
        }).then(function(json) {
 
            //alert(JSON.stringify(json));
            if(json.data.address && json.data.address.state) {

                var reversed_city = "unkown";

                var reversed_region = json.data.address.state.toLowerCase();

                if(json.data.address.city) {
                  reversed_city = json.data.address.city.toLowerCase();
                } else if(json.data.address.village) {
                  reversed_city = json.data.address.village.toLowerCase();
                } else if(json.data.address.county) {
                  reversed_city = json.data.address.county.toLowerCase();
                }

                me.currentGeodecodedData = {
                  lat: me.currentPosition.coords.latitude,
                  lng: me.currentPosition.coords.longitude,
                  citta: reversed_city,
                  region: reversed_region
                };

                //alert(JSON.stringify(me.currentGeodecodedData));
                onComplete(null, me.currentGeodecodedData);

            } else {

                onComplete('invalid data returned from service', null);

            }


        }, function(error, xhr) {

          onComplete(JSON.stringify(error), null);

        });

    };

    me.watchPosition = function() {

        //alert("GeoLocationService - watchPosition");
        console.log("GeoLocationService - watchPosition");

        navigator.geolocation.getCurrentPosition(function(position) {

            me.onSuccessWatch(position);

        }, function(error) {

            me.onErrorWatch(error);

        }, {

            timeout: me.timeout,
            enableHighAccuracy: me.enableHighAccuracy,
            maximumAge: me.maximumAge

        });

    }


})

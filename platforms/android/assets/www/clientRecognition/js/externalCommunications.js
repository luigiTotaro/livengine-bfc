function requestUsersNumber()
{
  AR.platform.sendJSONObject({
    topic: "getUsersNumber"
  })
}

//ritorno dalla funzione di sopra
function getUsersNumber(numero)
{
  $("#badgeUtenti p").html(numero);
  $("#badgeUtenti").show();
}

function openContent(idImmagine,nome)
{
    AR.platform.sendJSONObject({
      topic: "openContent",
      id: idImmagine,
      titolo: nome
    });
    /*
    //document.location = 'architectsdk://openContent_' + encodeURIComponent(JSON.stringify({
      id: idImmagine,
      titolo: nome
    }));
*/
}

function hotspotClick(idImmagine, nome, id_hotspot, hsLabel, extra)
{
    AR.platform.sendJSONObject({
      topic: "hotspotClick",
      id: idImmagine,
      titolo: nome,
      id_hotspot: id_hotspot,
      hotspot: hsLabel,
      extraData: extra
    });
}

function requestConnectionStatus()
{
  AR.platform.sendJSONObject({
    topic: "checkConnessione"
  })

  //document.location = 'architectsdk://checkConnessione';
}

function openExternalLink(url)
{
  AR.platform.sendJSONObject({
    topic: "linkEsterno",
    url: url //è già encodato....
  });  
  //document.location = 'architectsdk://linkEsterno_'+url;
}

function existSlideshow(id)
{
  AR.platform.sendJSONObject({
    topic: "existSlideshow",
    id: id,
  });
  //document.location = 'architectsdk://existSlideshow_'+id;
}

function scaricaHtmlZip(jsonConfig)
{
  //non implementato al momento
  //document.location = 'architectsdk://scaricaHtmlZip_'+jsonConfig;
}

function exitRA()
{
  accendiLed (false);
  AR.platform.sendJSONObject({
    topic: "exit"
  })
  //document.location = 'architectsdk://index.html';
}

//ritorno dalla richiesta di scaricare una immagine
function downloadElementStatus(testo)
{
  $("#loader").hide();

  if (testo=="ok") testo=getLabel("CONTENUTO_SALVATO","it","Contenuto salvato");
  $("#messaggioGenerico p").html(testo);
  $("#messaggioGenerico").show();

  setTimeout(function() {
    $("#messaggioGenerico").hide();
  }, 4000);

  back();
}

function navigaVersoPosizione(lat, lon)
{
  AR.platform.sendJSONObject({
    topic: "naviga",
    lat: lat,
    lon: lon
  });
/*
  //document.location = 'architectsdk://naviga_' + encodeURIComponent(JSON.stringify({
    lat: lat,
    lon: lon
  }));
*/
}


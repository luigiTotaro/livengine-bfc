angular.module('LiveEngine.LazyLoader.Services', [])

.service('LazyLoader', function(HelperService, GlobalVariables, FileSystemService, LoggerService, $rootScope, $http, $ionicModal, $ionicPopup) {

    var me = this;
 
    me.appConfiguration = {
      showUsersGuideAtStartup: false,
      showDisclaimerAtStartup: false,
      showDisclaimerForComments: true,
      commentsNickname: ""
    };

    me.classes = [];
    me.__chunkSize = 5 * 1024 * 1024;


    me.isClassLoaded = function(className) {
        return !me.classes[className] == null;
    };


    // parameters.onComplete -> function(err)
    me.saveConfiguration = function(parameters) {

        FileSystemService.writeFile({
            directory: GlobalVariables.directory_main,
            fileName: 'configuration.json',
            bufferData: JSON.stringify(me.appConfiguration),
            onComplete: function(err, operationCompleted) {

                if(err) {
                    parameters.onComplete(err);
                } else {

                    if(operationCompleted == false) {
                        parameters.onComplete("save file definition not completed");
                    } else {
                        parameters.onComplete(null);
                    }
                }

            }

        });

    }



    // parameters.onComplete -> function(err)
    me.loadConfiguration = function(parameters) {

        FileSystemService.fileExists({
            directory: GlobalVariables.directory_main,
            fileName: 'configuration.json',
            onComplete: function(err, isAvailable) {

                if(err) {

                    parameters.onComplete(err);

                } else {

                    if(!isAvailable) {

                        parameters.onComplete(null);

                    } else {

                        FileSystemService.fileRead({
                            directory: GlobalVariables.directory_main,
                            fileName: 'configuration.json',
                            onComplete: function(err, bufferData) {

                                try {

                                    if(err)
                                      throw err;

                                    me.appConfiguration = bufferData ? JSON.parse(bufferData) : {};
                                    //default per eventuali variabili non incluse nel file di configurazione
                                    if (typeof me.appConfiguration.showDisclaimerForComments == 'undefined') me.appConfiguration.showDisclaimerForComments = true;
                                    if (typeof me.appConfiguration.commentsNickname == 'undefined') me.appConfiguration.commentsNickname = "";

                                    parameters.onComplete(null);

                                } catch(err) {

                                    parameters.onComplete(err);

                                }

                            }
                        });

                    }
                }
            }
        });

    };




    me.Init = function(parameters) {

        FileSystemService.InitializeFilesystem({
            onComplete: function(err) {

                if(err) {
                    parameters.onComplete(err);
                } else {

                    //parameters.onComplete(null);
                    me.loadConfiguration({
                        onComplete: function(err) {
                            parameters.onComplete(err);
                        }
                    });


                }

            }

        });

    };



    // parameters.fileName
    // parameters.onComplete = function(err, isAvailable, localUrl)
    me.isFileAvailableLocally = function(parameters) {

      if(!window.cordova) {

          parameters.onComplete('isBrowser', false, null);

      } else {

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {

            fs.root.getDirectory(GlobalVariables.directory_main + '/dinamic_classes' + GlobalVariables.directory_trail, { create: false }, function (dinamicClassesDirEntry) {

                dinamicClassesDirEntry.getFile(

                    parameters.fileName,

                    { create: false },

                    function(fileEntry) {

                        parameters.onComplete(null, true, fileEntry.toURL());

                    },

                    function() {

                        parameters.onComplete(null, false, null);

                    }

                );

            }, function(err) {

                parameters.onComplete(evt.target.error.code, false, null);

            });


          }, function(err) {

              parameters.onComplete(evt.target.error.code, false, null);

          });


      }

    }


    // parameters.fileName
    // parameters.libraryUrl
    // parameters.onComplete = function(err, localUrl)
    me.downloadFileLibrary = function(parameters) {

        window.requestFileSystem(LocalFileSystem.PERSISTENT, me.__chunkSize, function (fs) {

            fs.root.getDirectory(GlobalVariables.directory_main + '/dinamic_classes' + GlobalVariables.directory_trail, { create: true }, function (dinamicClassesDirEntry) {

                // Parameters passed to getFile create a new file or return the file if it already exists.
                dinamicClassesDirEntry.getFile(parameters.fileName, { create: true, exclusive: false }, function (fileEntry) {

                    var fileTransfer = new FileTransfer();
                    var fileURL = fileEntry.toURL();

                    fileTransfer.download(
                      parameters.libraryUrl,
                      fileURL,

                      function (entry) {
                          parameters.onComplete(null, fileURL);
                      },

                      function (error) {
                          parameters.onComplete(error.code, null);
                      }

                  );

                }, function(err) {
                    parameters.onComplete("file get err: " + JSON.stringify(err), null);
                });

            }, function(err) {
              parameters.onComplete(JSON.stringify(err), null);
            });

        }, function(err) {
            parameters.onComplete(evt.target.error.code, null);
        });



    };



    me.doRequire = function(filePath, onComplete) {

      try {

        requirejs(
            [ filePath ],
            function() {
                onComplete(null);
            },
            function(err) {
                onComplete(err);
            }
        );

      } catch(err) {

          onComplete(err);

      }

    };

    // parameters.fileName (es: MapClass.js)
    // onComplete: function(err)
    me.loadLibrary = function(parameters) {


        if(!window.cordova) {

            me.doRequire(GlobalVariables.application.applicationUrl + '/' + parameters.fileName + '/library/get', function(err) {
                parameters.onComplete(err);
            });

        } else {

            if(me.classes[parameters.fileName]) {

                parameters.onComplete(null);

            } else {

                try {

                      if( HelperService.isNetworkAvailable() == true ) {

                          me.downloadFileLibrary({
                              fileName: parameters.fileName,
                              libraryUrl: GlobalVariables.application.applicationUrl + '/' + parameters.fileName + '/library/get',
                              onComplete: function(err, localFileUrl) {

                                  if(err) {

                                      // non riesco a fare un download. prima di restituire un errore tento un download diretto da server
                                      me.doRequire(GlobalVariables.application.applicationUrl + '/' + parameters.fileName + '/library/get', function(err) {
                                          if(err) {
                                              parameters.onComplete(err);
                                          } else {
                                              me.classes[parameters.fileName] = true;
                                              parameters.onComplete(null);
                                          }
                                      });

                                  } else {

                                      me.doRequire(localFileUrl, function(err) {

                                          if(err) {
                                              parameters.onComplete(err);
                                          } else {
                                              me.classes[parameters.fileName] = true;
                                              parameters.onComplete(null);
                                          }

                                      });

                                  }

                              }

                          });

                      } else {

                          me.isFileAvailableLocally({
                              fileName: parameters.fileName,
                              onComplete: function(err, isAvailable, localFileUrl) {

                                  if(err || !isAvailable) {

                                      parameters.onComplete(err ? err : 'file non disponibile localmente');

                                  } else {

                                      me.doRequire(localFileUrl, function(err) {


                                          if(err) {
                                              parameters.onComplete(err);
                                          } else {
                                              me.classes[parameters.fileName] = true;
                                              parameters.onComplete(null);
                                          }

                                      });

                                  }


                              }
                          });


                      }

                } catch(err) {

                    //alert("Errore grave [" + err + "]");
                    alert("Errore grave [COD: 1001]");

                }

            }

        }

    };


    // parameters.fileName (es: MapClass.js)
    // onComplete: function(err)
    me.loadARLibrary = function(parameters) {


        if(!window.cordova)
        {
          parameters.onComplete(null);
        }
        else
        {

          try {

                if( HelperService.isNetworkAvailable() == true ) {

                    me.downloadFileLibrary({
                        fileName: parameters.fileName,
                        libraryUrl: GlobalVariables.application.applicationUrl + '/' + parameters.fileName + '/ARlibrary/get',
                        onComplete: function(err, localFileUrl) {

                            if(err) {

                                // non riesco a fare un download. prima di restituire un errore vedo se il file � disponibile localmente
                                me.isFileAvailableLocally({
                                    fileName: parameters.fileName,
                                    onComplete: function(err, isAvailable, localFileUrl) {

                                        if(err || !isAvailable) {

                                          parameters.onComplete(err ? err : 'file non disponibile localmente');

                                        } else {

                                          //� in locale, pronto per essere utilizzato nella RA
                                          parameters.onComplete(null);

                                        }


                                    }
                                });




                            } else {

                                //scaricato, pronto per essere utilizzato nella RA
                                parameters.onComplete(null);

                            }

                        }

                    });

                }
                else
                {

                    me.isFileAvailableLocally({
                        fileName: parameters.fileName,
                        onComplete: function(err, isAvailable, localFileUrl) {

                            if(err || !isAvailable) {

                                parameters.onComplete(err ? err : 'file non disponibile localmente');

                            } else {

                                //� in locale, pronto per essere utilizzato nella RA
                                parameters.onComplete(null);

                            }


                        }
                    });


                }

          }
          catch(err)
          {
              //alert("Errore grave [" + err + "]");
              alert("Errore grave [COD: 1002]");
          }

      }

    };






})

angular.module('LiveEngine.SocketService.Services', [])

.service('SocketService', function(GlobalVariables, LoggerService,
FileSystemService, $rootScope, $http, $ionicModal, $ionicPopup) {

    var self = this;
    self.socket = null;
    self.onSocketOpen = null;
    self.last_communication_time = [];
    self.last_communication_limit = [];

    // delta di inattività per singolo messaggio //
    // 500 secondi
    self.last_communication_limit['set_device_data'] = 500;

    // 10 secondi
    self.last_communication_limit['set_login'] = 10;

    // 30 secondi
    self.last_communication_limit['has_open_augmented_content'] = 5;

    self.last_communication_limit['has_opened_map'] = 30;

    //self.last_communication_limit['has_view_progetto'] = 20;



    self.callbacks = [];


    self.setCallback = function(callbackName, messageType, callback) {

        if(!self.callbacks[messageType]) {
            self.callbacks[messageType] = [];
        }

        delete self.callbacks[messageType][callbackName];

        if(callback) {
            self.callbacks[messageType][callbackName] = callback;
        }

    }



    self.openSocket = function(onConnect) {

        self.onSocketOpen = onConnect;

        if(self.socket) {
            return;
        }


        try {

          self.socket = io(GlobalVariables.application.applicationUrl, {'force new connection': true });

          self.socket.on('connect', function() {

              self.onSocketOpen();

          });


          self.socket.on('server_message', function(message) {

              if(!(message.type && message.msg_body)) {
                return;
              }


              for(var callbackName in self.callbacks[message.type]) {
                  self.callbacks[message.type][callbackName](message.msg_body);
              }

          });

          self.socket.on('disconnect', function() {
              console.log('sono disconnesso dal serever');
          });

        } catch(err) {
          //alert(err);
        }

    }


    self.closeSocket = function() {

        if(self.socket) {

          try {
              self.socket.disconnect();
              self.socket = null;
          } catch(err) {
              self.socket = null;
          }

        }

    };








    self.emit = function(event_name, message_type, options, payload) {

        if(!self.socket) {
          return;
        }

        var NOW = (new Date()).getTime();


        if(self.last_communication_time[message_type] && self.last_communication_limit[message_type]) {

            DELTA = (NOW - self.last_communication_time[message_type]) / 1000;

            if(DELTA < self.last_communication_limit[message_type]) {
                return;
            }

        }

        self.last_communication_time[ message_type ] = NOW;

        self.socket.emit(event_name, {
            type: message_type,
            msg_body: payload
        });

    };


});

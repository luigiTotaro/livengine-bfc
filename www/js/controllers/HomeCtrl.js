angular.module('LiveEngine.Home.Controller', [])


.controller('HomeCtrl', function(GlobalVariables, SupportServices, $scope
, $rootScope, $state, $ionicModal, $http, LazyLoader, HelperService, $ionicLoading
, $ionicPopup, LoggerService, $timeout, FileSystemService, $q, SocketService, $stateParams, DeviceLogService, LanguageService, WikiService_copertina, WikiService, GeoLocationService, PushServices, GameCenter) {

  var me = this;

  $scope.data = {
    testate: [],
    labels: {
      title: LanguageService.getLabel('SCEGLI_TESTATA')
    },
    timer: null,
    //dalla pagina TestataDetail, non piu' presente ora
    testata: null,
    disableBackButton: false,
    layout: null,
    layoutCaricato: false,
    classiDinamicheCaricate: false,
    classiDinamicheARCaricate: false,
    numClassiTotaliCaricate: 0,
    numClassiTotaliARCaricate: 0,
    nodeIpAddresses: [],
    monotestata: false

  };

  $scope.clickOnItem = function(testata) {
      if(!window.cordova) {
          $scope.clickOnItem_afterCheck(testata);

      } else {

        //controllo permesso fotocamera ios
        if (ionic.Platform.isIOS())
        {
          cordova.plugins.diagnostic.getCameraAuthorizationStatus(
              function(status){
                      if (status==cordova.plugins.diagnostic.permissionStatus.DENIED)
                      {
                        var messaggio=LanguageService.getLabel('PERMESSO_FOTOCAMERA_REVOCATO');
                        messaggio = messaggio.replace("XXXXXX", "BFC AR");
                        //è stato revocato, dall'utente o per altre ragioni.... devo richiedrlo nuovamente
                        HelperService.showGenericMessage(messaggio, true); 
                        DeviceLogService.log("ERROR","PERMESSO_FOTOCAMERA_REVOCATO");
                      }
                      else
                      {
                        $scope.clickOnItem_afterCheck(testata);
                      }
                      //alert("Camera grant is: " + status);
              }, function(error){
                  //faccio partire comunque
                  $scope.avviaRealtaAumentataCopertina_afterCheck();
              }, false
          );                     
        }
        else //android
        {
          $scope.clickOnItem_afterCheck(testata);
        }
    }
  };

  $scope.clickOnItem_afterCheck = function(testata) {
      //console.log(testata);
      $scope.gotoDetailPage(testata);
  };


  $scope.gotoDetailPage = function(testata) {

      if(!window.cordova) {
          $scope.gotoDetailPageOK(testata);

      } else {

        cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, function( status ){
          if ( status.hasPermission ) {
            console.log("WRITE_EXTERNAL_STORAGE Yes :D ");
            $scope.gotoDetailPageOK(testata);
          }
          else {
            console.warn("WRITE_EXTERNAL_STORAGE No :( ");
            var messaggio=LanguageService.getLabel('PERMESSO_SCRITTURA_NEGATO');
            messaggio = messaggio.replace("XXXXXX", "BFC AR");
            HelperService.showGenericMessage(messaggio, true); 
            DeviceLogService.log("ERROR","PERMESSO_SCRITTURA_NEGATO");
          }
        });
      }
      
      /*
      cordova.plugins.permissions.requestPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, function success( status ) {
        if( !status.hasPermission ) console.warn('non ha dato il permesso');
        else (console.warn('Ha dato il permesso'))
      }, function error() {
        console.warn('errore durante il request permission');
      }); 
*/
  };


  $scope.gotoDetailPageOK = function(testata) {

      GlobalVariables.application.currentTestata = testata;
      GlobalVariables.application.applicationUrl = testata.socketUrl;
      //var numeroUtenti=testata.utentiTestata;
      clearInterval($scope.data.timer);

      //parte spostata dalla pagina TestataDetail
      $ionicLoading.show({
          template: '<div style="padding:10px 5px;">'+LanguageService.getLabel('ATTENDERE')+'</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>'
      });

      /* for testing only  */
      //GlobalVariables.application.applicationUrl = 'http://192.170.5.25:3030/' + GlobalVariables.application.applicationUrl.substring( GlobalVariables.application.applicationUrl.lastIndexOf("/") + 1);

      console.log(GlobalVariables.application.applicationUrl);

      $scope.data.testata = GlobalVariables.application.currentTestata;
      $scope.data.disableBackButton = GlobalVariables.application.disableBackButtonToHome;

      SupportServices.getLayoutTestata({
          idTestata: $scope.data.testata.idTestata,
          onComplete: function(err, jsonLayout) {

              if(err) {

                  HelperService.showUnrecoverableError(err, true);
                  $ionicLoading.hide();

              } else {

                  $timeout(function() {
                    jsonLayout.homeLogo = GlobalVariables.baseUrl + "/" + jsonLayout.homeLogo;
                    jsonLayout.homeSfondo = GlobalVariables.baseUrl + "/" + jsonLayout.homeSfondo;
                    jsonLayout.homeStart = GlobalVariables.baseUrl + "/" + jsonLayout.homeStart;

                    GlobalVariables.application.currrentLayoutTestata = jsonLayout;
                    $scope.data.layout = jsonLayout;

                    $scope.checkCaricamentoClassi (true, null, null, null,null);
                  });

              }

          }

      });


      SupportServices.getLibrariesToLoad({
          idTestata: $scope.data.testata.idTestata,
          onComplete: function(err, jsonLibs) {
              if(!err) {
                //console.log(jsonLibs.length);

                _.each(jsonLibs, function(libName) {

                      //console.log(libName);
                      var className = libName.replace(".js", "");

                      LazyLoader.loadLibrary({

                          fileName: libName,

                          onComplete: function(err) {

                              if(!err) {

                                  try {

                                      window[className].Initialize({
                                          ionicModal: $ionicModal,
                                          ionicPopup: $ionicPopup,
                                          http: $http,
                                          rootScope: $rootScope,
                                          GlobalVariables: GlobalVariables,
                                          HelperService: HelperService,
                                          LoggerService: LoggerService,
                                          FileSystemService: FileSystemService,
                                          WikiService: WikiService,
                                          ionicLoading: $ionicLoading,
                                          SocketService: SocketService,
                                          LanguageService: LanguageService,
                                          GameCenter: GameCenter,
                                          RootScope: $rootScope
                                      });
                                      
                                      $scope.checkCaricamentoClassi (null, jsonLibs.length, true, null,null);



                                  } catch(err) {

                                    console.log("error loading " + err);

                                  }

                              }
                          }

                      });

                });
              }
              else
              {
                HelperService.showUnrecoverableError(err, true);
                $ionicLoading.hide();
              }
          }

      });

      SupportServices.getARLibrariesToLoad({
          idTestata: $scope.data.testata.idTestata,
          onComplete: function(err, jsonLibs) {
              if(!err) {
                
                //console.log("sono nel complete: jsonLibs:");
                //console.log(JSON.stringify(jsonLibs));

                _.each(jsonLibs, function(libName) {

                      console.log(libName);
                      var className = libName.replace(".js", "");

                      LazyLoader.loadARLibrary({

                          fileName: libName,

                          onComplete: function(err) {

                              if(!err) {
                                //console.log("caricato: " + libName);
                                $scope.checkCaricamentoClassi (null, null, null, jsonLibs.length, true);
                                //console.log("cazzo...");
                              }
                          }

                      });

                });
              }
              else
              {
                console.log("sono nell'error durante il caricamento delle classi dinamiche AR");
                $scope.checkCaricamentoClassi (null, null, null, 1, true); //così do per caricate le classi dinamiche AR

              }
          }

      });

      $scope.connect2Socket();
      //$state.go('testata_detail');


  };

  $scope.checkCaricamentoClassi = function(layout, numClassiTotali, classeCaricata, numClassiARTotali,classeARCaricata) {
    //console.log("sono in checkCaricamentoClassi ("+layout+","+numClassiTotali+","+classeCaricata+","+numClassiARTotali+","+classeARCaricata+")");
    //ha caricato il layout?
    if (layout==true)
    {
      $scope.data.layoutCaricato=true;
      console.log("Layout caricato");
    }
    if (classeCaricata==true)
    {
      $scope.data.numClassiTotaliCaricate++;
      if ($scope.data.numClassiTotaliCaricate==numClassiTotali) $scope.data.classiDinamicheCaricate=true;
      console.log("Classe dinamica "+$scope.data.numClassiTotaliCaricate+" caricata");
    }
    if (classeARCaricata==true) 
    {
      $scope.data.numClassiTotaliARCaricate++;
      if ($scope.data.numClassiTotaliARCaricate==numClassiARTotali) $scope.data.classiDinamicheARCaricate=true;
      console.log("Classe dinamica AR "+$scope.data.numClassiTotaliARCaricate+" caricata");
   }

    //controllo
    if (($scope.data.layoutCaricato==true) && ($scope.data.classiDinamicheCaricate==true) && ($scope.data.classiDinamicheARCaricate==true))
    {
      console.log("Caricato tutto!");
      $ionicLoading.hide();
      $scope.avviaRealtaAumentataCopertina();
    }
  }

  $scope.goPoi = function() {

      $state.go('poi_detail');
  }

  $scope.avviaRealtaAumentataCopertina = function() {

      //primo check... devo riconoscere la testata o vado direttamente all'ultimo (unico) numero della testata, o alla lista dei progetti?
      if(GlobalVariables.application.currentTestata.riconoscimento==0) //vado all'ultimo progetto
      {
        console.log("Non devo riconoscere la copertina");
        GlobalVariables.application.currentProgetto = {
          idProgetto: GlobalVariables.application.currentTestata.idProgetto
        };

        $ionicLoading.show({
            template: LanguageService.getLabel('ATTENDERE')
        });

        SupportServices.sceltaProgetto({
            onComplete: function(err, json) {

                $ionicLoading.hide();
                $rootScope.showHome=true;
                if(err) {

                    var alertPopup = $ionicPopup.alert({
                       title: LanguageService.getLabel('ATTENZIONE'),
                       template: LanguageService.getLabel('PROBLEMI_RETE')
                     });

                    return;
                }

                $state.go('progetto_detail');

            }

        });
      }  
      else if(GlobalVariables.application.currentTestata.riconoscimento==2) //vado alla lista dei progetti
      {
        console.log("Devo andare alla lista dei progetti");
        $state.go('navigazione_progetti');
      }
      else //faccio aumentare la copertina
      {
        console.log("Devo riconoscere la copertina");

        if(!window.cordova) {

            $state.go('navigazione_progetti');

        } else {

            var numUtenti=GlobalVariables.application.currentTestata.utentiTestata;
            var colore=GlobalVariables.application.currrentLayoutTestata.coloreGenerale;

            //alert(numUtenti);
            //alert(colore);

            $ionicLoading.show({
                template: '<div style="padding:10px 5px;">'+LanguageService.getLabel('AVVIO_REALTA_AUMENTATA')+'</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
                duration: 10000
            });

            //devo scaricare il wtc di riconoscimento di quella testata
            console.log("devo scaricare il wtc di riconoscimento della testata");

            var uri = encodeURI(GlobalVariables.baseUrl + "/wtcTestate/"+$scope.data.testata.idTestata+".wtc");
            var name = "testata_"+$scope.data.testata.idTestata+".wtc";
            var nameOnDevice = "testata_"+$scope.data.testata.idTestata+".mac";
            var localpath = FileSystemService.getLocalPath() + SupportServices.BUFFER_DIRECTORY;
            var fullName="wtc/"+nameOnDevice;
            var baseUrl = GlobalVariables.baseUrl;

            //è presente la connessione?
            console.log("Check connessione: " + HelperService.isNetworkAvailable());
            if (HelperService.isNetworkAvailable()==false)
            {
              console.log("Nessuna connessione, vedo se il file è comunque presente");
              FileSystemService.fileExists({
                  directory: SupportServices.BUFFER_DIRECTORY + '/wtc',
                  fileName: nameOnDevice,
                  onComplete: function(err, isAvailable) {

                      if(err) {
                          //errore, getto la spugna
                          console.log("Errore, esco");
                          HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);

                      } else {

                          if(!isAvailable) {
                               //non esiste, getto la spugna
                              console.log("Non Esiste, esco");
                              HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);

                          } else {
                              //esiste, vado avanti
                              console.log("Esiste, vado avanti");
                              var parameter = { "path": "www/clientRecognition/indexCopertina.html", "requiredFeatures": ["image_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localpath, "wtc":fullName, "baseUrl": baseUrl, "numUtenti": numUtenti, "colore": colore};
                             // var parameter = { "path": "www/clientRecognition/indexCopertina.html", "requiredFeatures": ["2d_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localpath, "wtc":fullName};
                              console.log(JSON.stringify(parameter));
                              WikiService_copertina.loadARchitectWorld(parameter);
                          }
                      }
                  }
              });
            }
            else
            {

              console.log("Provo a scaricare " + uri);

              FileSystemService.downloadFile({
                  url: uri,
                  directory: SupportServices.BUFFER_DIRECTORY + '/wtc',
                  fileName: nameOnDevice,
                  onComplete: function(err) {

                      if(err) {
                        console.log("download error: " + err);
                        console.log("Controllo se è comunque presente sul device");
                        //non è riuscito, vedo se ho comunque il file sul device
                        FileSystemService.fileExists({
                            directory: SupportServices.BUFFER_DIRECTORY + '/wtc',
                            fileName: nameOnDevice,
                            onComplete: function(err, isAvailable) {

                                if(err) {
                                    //errore, getto la spugna
                                    console.log("Errore, esco");
                                    HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);

                                } else {

                                    if(!isAvailable) {
                                         //non esiste, getto la spugna
                                        console.log("Non Esiste, esco");
                                        HelperService.showUnrecoverableError(LanguageService.getLabel('IMPOSSIBILE_SCARICARE_DATI'), true);

                                    } else {
                                        //esiste, vado avanti
                                        console.log("Esiste, vado avanti");
                                        //var parameter = { "path": "www/clientRecognition/indexCopertina.html", "requiredFeatures": ["2d_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localpath, "wtc":fullName};
                                        var parameter = { "path": "www/clientRecognition/indexCopertina.html", "requiredFeatures": ["image_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localpath, "wtc":fullName, "baseUrl": baseUrl, "numUtenti": numUtenti, "colore": colore};
                                        console.log(JSON.stringify(parameter));
                                        WikiService_copertina.loadARchitectWorld(parameter);


                                    }
                                }
                            }
                        });

                      } else {

                        console.log("download completato");
                        var parameter = { "path": "www/clientRecognition/indexCopertina.html", "requiredFeatures": ["image_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localpath, "wtc":fullName, "baseUrl": baseUrl, "numUtenti": numUtenti, "colore": colore};
                        //var parameter = { "path": "www/clientRecognition/indexCopertina.html", "requiredFeatures": ["2d_tracking"], "startupConfiguration": { "camera_position": "back"}, "localPath": localpath, "wtc":fullName};
                        console.log(JSON.stringify(parameter));
                        WikiService_copertina.loadARchitectWorld(parameter);
                      }
                  }
              });
            }

            

        }
    } //fine if riconoscimento si-no
  };



    $scope.$on("$ionicView.beforeEnter", function(event, data) {

        
        //console.log("Check socialsharing");
        //console.log(window.plugins.socialsharing);


        //se è monotestata, di default nascondo la home... se poi deve mostrare i poi, la riattivo
        console.log("Check");
        if (GlobalVariables.monotestata==1)
        {
          console.log("GlobalVariables.monotestata");
          $scope.data.monotestata=true;
          //per adesso lo rendo invisibile, dopo se ha i poi attivati lo rendo visibile
          $rootScope.showHome=false;
        }
        else
        {
          $scope.data.monotestata=false;
          $rootScope.showHome=true;
        }
        
        // elimino il tema grafico corrente così da poterlo ricaricare
        // dopo aver selezionato una testata
        GlobalVariables.application.currrentLayoutTestata = null;

        $scope.data.testate = GlobalVariables.application.listaProgetti;
        $scope.data.poi = GlobalVariables.application.poi;

        for(var j in $scope.data.testate) {
            $scope.data.testate[ j ].backgroundLastNumber = GlobalVariables.baseUrl + "/" + $scope.data.testate[j].immagineProgetto;
            $scope.data.testate[ j ].utentiTestata=0;
        
            //mi creo un array di indirizzi node a cui spedire l'aggiornamento della push id
            var nodeAddress=$scope.data.testate[j].socketUrl;
            nodeAddress=nodeAddress.substring(0, nodeAddress.lastIndexOf("/"));
            //console.log("recuperato un indirizzo node: " + nodeAddress);
            //se non esiste nell'array, lo inserisco
            if ($scope.data.nodeIpAddresses.indexOf(nodeAddress) == -1)
            {
              //console.log("non esiste nell'array, lo inserisco");
              $scope.data.nodeIpAddresses.push(nodeAddress);
            }
            //console.log("array socket aggiornato: ");
            //console.log($scope.data.nodeIpAddresses);


        }
        console.log("finito - array socket finale: ");
        console.log($scope.data.nodeIpAddresses);

        //console.log($scope.data.testate);

        $scope.getUser4Socket(0); //primo elemento
        //istanzio un timer che mi fa vedere le variazioni, ma solo una volta
        $scope.data.timer = setTimeout($scope.getUser4Socket, (GlobalVariables.intervalloUtentiTestate*1000));

        //se non ho aggiornato il server node con il push id di questo device, lo faccio
        if(window.cordova) {
          if (!GlobalVariables.application.sendPushId2ServerSent)
          {
            GlobalVariables.application.sendPushId2ServerSent=true;
            

            //mando la richiesta al server
            //alert(PushServices.PUSH_NOTIFICATION_REGISTRATION_ID)
            //alert(GlobalVariables.deviceUUID)
            //alert(GlobalVariables.packageName)
            //alert("Mando la richiesta al server..")


            SupportServices.sendPushId2Server({
              url: $scope.data.nodeIpAddresses[0],
              //url: "http://192.170.5.25:3030",
              pushId: PushServices.PUSH_NOTIFICATION_REGISTRATION_ID,
              uid: GlobalVariables.deviceUUID,
              packageName: GlobalVariables.packageName,
              onComplete: function(err, json_data) {
                
                if(err) {
                  console.log("Errore dal setPushId: ");
                  console.log(err);
                }
                else
                {
                  console.log("Tutto OK dal setPushId: ")
                  console.log(json_data);
                }

              }

            });
            

          }
          else
          {
            //alert("Richiesta già mandata al server..")
          } 
        }        
        

    });

    $scope.showMessage = function() {
        if (GlobalVariables.application.comingFrom == "START")
        {
          if (GlobalVariables.message2show_text!="") HelperService.showGenericMessage(GlobalVariables.message2show_text, true);
        }
        GlobalVariables.application.comingFrom == "";   
    }

    $scope.$on("$ionicView.afterEnter", function(event, data) {
        $timeout($scope.showMessage,1000);

        LoggerService.triggerAction({
            action: LoggerService.ACTION.ENTER,
            state: LoggerService.CONTROLLER_STATES.HOME,
            data: null
        });


        //prova di invio file di log nel db
        DeviceLogService.sendLogFile({
            onComplete: function(err, res) {

                if(err) {
                    //console.log("Errore, esco");
                    //console.log(err);
                } else {
                    //console.log("tutto ok");
                    //console.log(res);

                }

                /*
                //aspetto almeno che abbia scritto il file di log, prima di scrivere altri log...
                DeviceLogService.log("INFO","Entrato nella home - Test INFO");
                setTimeout(function() {
                  DeviceLogService.log("WARN","Entrato nella home - Test WARN");
                }, 1000);                
                setTimeout(function() {
                  DeviceLogService.log("ERROR","Entrato nella home - Test ERROR");
                }, 2000);
                */
    
            }
        });
        

        if (GlobalVariables.monotestata==1)
        {
          //vado direttamente all'unica testata, ma solo se NON ha i poi attivati....
          if ($scope.data.poi == null)
          {
            unicaTestata=$scope.data.testate[0];
            console.log(unicaTestata);
            $scope.clickOnItem(unicaTestata)
          }
          else
          {
            //mostro tutto
            $rootScope.showHome=true;
          }
        }



        //share test

        
        //FB
        
        //ios - funziona ma restituisce un errore "not available" dopo aver condiviso.
        //per ovviare, si vede prima se può condividere con canShareVia e poi se tutto ok si condivide fregandosene del messaggio di errore seguente

        //immagine
        /*
        window.plugins.socialsharing.canShareVia("com.apple.social.facebook", 'Message via fb', null, null , null, function(e)
        {
          window.plugins.socialsharing.shareViaFacebook('Message via Facebook', "http://livengine.mediasoftonline.com/contenuti/908_1_socialImg_1563791902141.jpg", null, function() {alert('share ok')}, function(errormsg){alert('share ok, più o meno')});

        }, function(e)
        {
            alert("KO: " + e);
        });
        */
        /*
        //link
        window.plugins.socialsharing.canShareVia("com.apple.social.facebook", 'Message via fb', null, null , null, function(e)
        {
          window.plugins.socialsharing.shareViaFacebook('Message via Facebook', null, "https://forbes.it/2019/07/20/diventare-astronauta-sogni-dei-bambini/", function() {alert('share ok')}, function(errormsg){alert('share ok, più o meno')});

        }, function(e)
        {
            alert("KO: " + e);
        });
        */
        
        //android
        //window.plugins.socialsharing.shareViaFacebook('Message via Facebook', "http://livengine.mediasoftonline.com/contenuti/908_1_socialImg_1563791902141.jpg", null /* url */, function() {alert('share ok')}, function(errormsg){alert(errormsg)});
        //android funziona, immagine

        //window.plugins.socialsharing.shareViaFacebook('Message via Facebook', null /* img */, "https://forbes.it/2019/07/20/diventare-astronauta-sogni-dei-bambini/", function() {alert('share ok')}, function(errormsg){alert(errormsg)});
        //android funziona, link

        //twitter
        //window.plugins.socialsharing.shareViaTwitter('Message via twitter', null /* img */, null /* link */, function() {alert('share ok')}, function(errormsg){alert(errormsg)});
        //solo testo - android ok - ios problema del not available

        //window.plugins.socialsharing.shareViaTwitter('Message via twitter', "http://livengine.mediasoftonline.com/contenuti/908_1_socialImg_1563791902141.jpg", null /* link */, function() {alert('share ok')}, function(errormsg){alert(errormsg)});
        //testo e immagine - android ok

        //window.plugins.socialsharing.shareViaTwitter('Message via twitter', null /* img */, "https://forbes.it/2019/07/20/diventare-astronauta-sogni-dei-bambini/", function() {alert('share ok')}, function(errormsg){alert(errormsg)});
        //testo e link - android ok

        //window.plugins.socialsharing.shareViaTwitter('Message via twitter', "http://livengine.mediasoftonline.com/contenuti/908_1_socialImg_1563791902141.jpg", "https://forbes.it/2019/07/20/diventare-astronauta-sogni-dei-bambini/", function() {alert('share ok')}, function(errormsg){alert(errormsg)});
        //testo link e immagine - android ok

        //window.plugins.socialsharing.shareViaInstagram('Message via Instagram', 'http://livengine.mediasoftonline.com/contenuti/908_1_socialImg_1563791902141.jpg', function() {alert('share ok')}, function(errormsg){alert(errormsg)})
        //instagram non funziona

/*
        var options = {
          //message: null, // not supported on some apps (Facebook, Instagram)
          //subject: null, // fi. for email
          //files: [decodeURIComponent(objPassed.image)], // an array of filenames either locally or remotely
          //url: decodeURIComponent(objPassed.link),
          //appPackageName: "com.facebook.katana" // Android only, you can provide id of the App you want to share with
          //appPackageName: "com.apple.social.facebook" // Android only, you can provide id of the App you want to share with
          appPackageName: "instagram" // Android only, you can provide id of the App you want to share with

        };

        //if (objPassed.image != "null") options.files = [decodeURIComponent(objPassed.image)];
        //if (objPassed.link != "null") options.url = decodeURIComponent(objPassed.link);

        options.files = ["http://livengine.mediasoftonline.com/contenuti/908_1_socialImg_1563791902141.jpg"];
        //options.url = "https://forbes.it/2019/07/20/diventare-astronauta-sogni-dei-bambini/";


        console.log("oggetto per fb:");
        console.log(JSON.stringify(options));


        var onSuccess = function(result) {
            console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
            console.log("Shared to app: " + result.app); // On Android result.app since plugin version 5.4.0 this is no longer empty. On iOS it's empty when sharing is cancelled (result.completed=false)
            //WikiService.showPopupReturn2AR("fb");
        };

        var onError = function(msg) {
          console.log("Sharing failed with message: " + msg);
            //WikiService.showPopupReturn2AR("fb");
        };

        window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
*/


      /*
      console.log("chiedo i permessi");
      cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.CAMERA, function( status ){
        if ( status.hasPermission ) {
          console.log("Camera Yes :D ");
        }
        else {
          console.warn("Camera No :( ");
        }
      });
      
      cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, function( status ){
        if ( status.hasPermission ) {
          console.log("WRITE_EXTERNAL_STORAGE Yes :D ");
        }
        else {
          console.warn("WRITE_EXTERNAL_STORAGE No :( ");
        }
      });

      cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.SEND_SMS, function( status ){
        if ( status.hasPermission ) {
          console.log("SEND_SMS Yes :D ");
        }
        else {
          console.warn("SEND_SMS No :( ");
        }
      }); 


      cordova.plugins.permissions.requestPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, function success( status ) {
        if( !status.hasPermission ) console.warn('non ha dato il permesso');
        else (console.warn('Ha dato il permesso'))
      }, function error() {
        console.warn('errore durante il request permission');
      });  
      */ 

    });

    $scope.$on("$ionicView.enter", function(event, data) {


        //$scope.showMessage();
    });


    $scope.$on("$ionicView.beforeLeave", function(event, data) {
        
        $ionicLoading.hide();

        LoggerService.triggerAction({
            action: LoggerService.ACTION.LEAVE,
            state: LoggerService.CONTROLLER_STATES.HOME,
            data: null
        });
    });

    $scope.getUser4Socket = function(index) {
      
      if ($scope.data.testate.length==0) return; //non ci sono testate, inutile chiedere i socket

      if (index==null) index=0;
      //console.log("sono in getUser4Socket con index=" + index);
      var actualSocket=$scope.data.testate[index].socketUrl;
      //console.log("prima del cambio=" + actualSocket);
     

      /* for testing only  */
      //actualSocket = 'http://192.170.5.25:3030/' + actualSocket.substring( actualSocket.lastIndexOf("/") + 1);
      //console.log("dopo il cambio=" + actualSocket);

      //console.log(actualSocket);

      SupportServices.getUsersBySocket({
          url: actualSocket,
          onComplete: function(err, json_data) {
            
            if(err) {

            }
            else
            {
              //console.log(json_data.totale);
              $scope.data.testate[index].utentiTestata=json_data.totale;
            }

            //console.log($scope.data.testate);
            index++;
            //console.log($scope.data.testate.length);
            //console.log(index);

            
            if (index>=$scope.data.testate.length)
            {
              //console.log("ridisegno");
            }
            else
            {
              //console.log("vado avanti");
              $scope.getUser4Socket(index);
            }


          }

      });

    }

    $scope.connect2Socket = function() {

            LoggerService.triggerAction({
            action: LoggerService.ACTION.ENTER,
            state: LoggerService.CONTROLLER_STATES.DETAIL_TESTATA,
            data: null
        });

        // apertura di una connessione socket. il socket coincide, lato server, con una istanza socket con namespace dedicato alla rivista
        SocketService.openSocket(function() {

            // se il device ha già un id apn/gcm la callback è eseguita immediatamente
            // altrimenti verrà eseguita quando l'utente autorizza le push notification
            PushServices.setOnRegisteredCallback(function(pushId) {

                setTimeout(function() {

                    SocketService.emit('message','set_device_data', {}, {
                        uid: GlobalVariables.deviceUUID,
                        pushId: window.cordova ? pushId : 'Browser'
                    });

                }, 5000);

            });


            var msg = {};

            msg.set_login = {
                uid: GlobalVariables.deviceUUID,
                os: window.cordova ? window.device.platform.toLowerCase() : 'Browser',
                version: window.cordova ? window.device.version : 'Browser',
                appVersion: GlobalVariables.version+"_"+GlobalVariables.build
            };

            if(GeoLocationService.getCurrentGeodecodedData()) {
                msg.position = GeoLocationService.getCurrentGeodecodedData();
            }

            if(GlobalVariables.application.currentProgetto) {
                msg.progetto = {
                    id: GlobalVariables.application.currentProgetto.idProgetto
                };
            }

            SocketService.emit('message','has_open_or_reopen_socket', {}, msg);

        });



        GeoLocationService.setCallback("updateSocketGps", function(position) {

            // un timer recupera la posizione del device on X minuti. se questo valore è valido lo comunico al server
            if(position) {
                //alert(JSON.stringify(position));
                GeoLocationService.reverseGeocode(function(err, geodecoded_data) {
                    if(geodecoded_data) {
                        //alert(JSON.stringify(geodecoded_data));

                        SocketService.emit('message','position', {}, geodecoded_data);
                    }
                });
            }


        });

    }


});
